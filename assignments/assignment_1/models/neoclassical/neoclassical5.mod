var c k i y;
varexo A;


parameters alpha beta delta sigma;
alpha = 0.3;
beta = 0.98;
delta = 0.025;
sigma = 1;

model;
c^(-sigma) = beta*c(+1)^(-sigma)*(alpha*A(+1)*k^(alpha-1)+1-delta);
c+i=y;
k = (1-delta)*k(-1)+i;
y = A*k(-1)^alpha;
end;

steady_state_model;
k = ((1-beta*(1-delta))/(beta*alpha*A))^(1/(alpha-1));
c = A*k^alpha-delta*k;
i = delta*k;
y = c+i;
end;

initval;
A=1;
end;

endval;
A=1.05;
end;

steady;

shocks;
var A;
periods 1:5;
values 1;
end;


perfect_foresight_setup(periods=100);
perfect_foresight_solver;

plot(simulation("k"))
plot(simulation("c"))
plot(simulation("i"))
plot(simulation("y"))

