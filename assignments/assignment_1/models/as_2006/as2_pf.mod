// An and Schorfheide model
// see An Schorfheide (2006) and Herbst Schorfheide (2016)
// and corrections https://cpb-us-w2.wpmucdn.com/web.sas.upenn.edu/dist/e/242/files/2017/05/corrections_v1-11vpcen.pdf
// Stationary nonlinear version, variables in relative gap to steady state
// Steady state is zero for all variables
// Calibration: An and Schorfheide (2006), Table 3 and page 38.
// Warning: by simplicity we use the same variable names as in as1.mod but the meaning is different
var c y pi R z g;
varexo eg ez eR;
parameters tau gamma phi nu pi_star r rho_R rho_z rho_g psi_1 psi_2 g_star;

tau = 2.0;
nu = 0.1;
phi = 53.68;
rho_R = 0.6;
rho_z = 0.75;
rho_g = 0.9;
psi_1 = 1.5;
psi_2 = 0.25;
r = 1.0025;
pi_star= 1.008;
gamma = 1.0055;
g_star = 1/0.85;

model;
  # beta = gamma/r;
  1 = exp(-tau*c(+1) + tau*c - z(+1) + R - pi(+1));
  0 = (exp(pi) - 1)*((1 - 1/(2*nu))*exp(pi) + 1/(2*nu))
      - beta*(exp(pi(+1)) - 1)*exp(-tau*c(+1) + tau*c + y(+1) - y + pi(+1))
      + ((1 - nu)/(nu*phi*pi_star^2))*(1 - exp(tau*c));
  exp(c - y) = exp(-g) - (phi*pi_star^2*g_star/2)*(exp(pi) - 1)^2;
  R = rho_R*R(-1) + (1 - rho_R)*(psi_1*pi
        + psi_2*(y - g)) + eR;
  g = rho_g*g(-1) + eg;
  z = rho_z*z(-1) + ez;
end;

steady;

shocks;
  var eg;
  periods 1;
  values 0.012;
  var eR;
  periods 1;
  values 0.004;
  var ez;
  periods 1;
  values 0.006;
end;

perfect_foresight_setup(periods=100);
perfect_foresight_solver;

plot(simulation("c"));
savefig("c.png")
plot(simulation("y"));
savefig("y.png")
plot(simulation("pi"));
savefig("pi.png")
plot(simulation("R"));
savefig("R.png")
