// An and Schorfheide model
// see An Schorfheide (2006) and Herbst Schorfheide (2016)
// and corrections https://cpb-us-w2.wpmucdn.com/web.sas.upenn.edu/dist/e/242/files/2017/05/corrections_v1-11vpcen.pdf
// Linearized version, variables in relative gap to steady state
// Steady state is zero for all variables
// Calibration: An and Schorfheide (2006), Table 3 and page 38.
// Warning: the variables are the same as in as2.mod but not as in as1.mod
// c doesn't appear explicitly
// Perfect foresight simulation
var y pi R z g;
varexo eg ez eR;
parameters tau gamma r kappa rho_R rho_z rho_g psi_1 psi_2;

tau = 2.00;
nu = 0.1;
phi = 53.68;
pi_star = 1.008;
kappa = tau*(1 - nu)/(nu*pi_star^2*phi);
rho_R = 0.6;
rho_z = 0.75;
rho_g = 0.9;
psi_1 = 1.5;
psi_2 = 0.25;
r = 1.0025;
gamma = 1.0055;

model;
  # beta = gamma/r;
  y = y(+1) - (1/tau)*(R - pi(+1) - z(+1))
      + g - g(+1);
  pi = beta*pi(+1) + kappa*(y - g);
  R = rho_R*R(-1) + (1 - rho_R)*(psi_1*pi
        + psi_2*(y - g)) + eR;
  g = rho_g*g(-1) + eg;
  z = rho_z*z(-1) + ez;
end;

steady;

shocks;
  var eg;
  periods 1;
  values 0.012;
  var eR;
  periods 1;
  values 0.004;
  var ez;
  periods 1;
  values 0.006;
end;

perfect_foresight_setup(periods=100);
perfect_foresight_solver;

plot(simulation("y"));
savefig("y.png")
plot(simulation("pi"));
savefig("pi.png")
plot(simulation("R"));
savefig("R.png")
