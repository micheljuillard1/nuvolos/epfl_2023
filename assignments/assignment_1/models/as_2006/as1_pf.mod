// An and Schorfheide model
// see An Schorfheide (2006) and Herbst Schorfheide (2016)
// and corrections https://cpb-us-w2.wpmucdn.com/web.sas.upenn.edu/dist/e/242/files/2017/05/corrections_v1-11vpcen.pdf
// Stationary nonlinear version, variables in level
// Calibration: An and Schorfheide (2006), Table 3 and page 38.
// Perfect foresight simulation

var c y y_star pi R R_star z g;
varexo eg ez eR;
parameters tau gamma phi nu pi_star r rho_R rho_z rho_g psi_1 psi_2 g_star;

tau = 2.0;
nu = 0.1;
phi = 53.68;
kappa = 0.33;
rho_R = 0.6;
rho_z = 0.75;
rho_g = 0.9;
psi_1 = 1.5;
psi_2 = 0.25;
r = 1.0025;
pi_star= 1.008;
gamma = 1.0055;
g_star = 1/0.85;

model;
  # beta = gamma/r;
  1 = beta*(c(+1)/c)^(-tau)*(1/(gamma*z(+1)))*R/pi(+1);
  1 = phi*(pi - pi_star)*((1 - 1/(2*nu))*pi + pi/(2*nu))
      - phi*beta*(c(+1)/c)^(-tau)*(y(+1)/y)
      *(pi(+1) - pi_star)*pi(+1)
      + (1/nu)*(1 - c^tau);
  ADD MISSING EQUATION HERE
  R = R_star^(1-rho_R)*R(-1)^rho_R*exp(eR);
  R_star = r*pi_star*(pi/pi_star)^psi_1*(y/y_star)^psi_2;
  log(g) = (1-rho_g)*log(g_star) + rho_g*log(g(-1)) + eg;
  log(z) = rho_z*log(z(-1)) + ez;
  y_star = (1 - nu)^(1/tau)*g;
end;

steady_state_model;
  pi = pi_star;
  R_star = r*pi_star;
  R = R_star;
  c = (1 - nu)^(1/tau);
  y_star = g_star*c;
  y = y_star;
  z = 1;
  g = g_star;
end;

steady;
  
shocks;
  var eg;
  periods 1;
  values 0.012;
  var eR;
  periods 1;
  values 0.004;
  var ez;
  periods 1;
  values 0.006;
end;

perfect_foresight_setup(periods=100);
perfect_foresight_solver;

plot(simulation("c"));
savefig("c.png")
plot(simulation("y"));
savefig("y.png")
plot(simulation("pi"));
savefig("pi.png")
plot(simulation("R"));
savefig("R.png")
