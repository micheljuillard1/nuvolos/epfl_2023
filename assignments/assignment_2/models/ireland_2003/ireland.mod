var y a k c i h;
varexo e;

parameters theta rho eta gam bet delta aa;

bet = 0.99;
delta = 0.025;
theta = 0.2;
rho = 0.9959; 
eta = 1.0051;  
gam = 0.0045;
aa = 1.8;

model;
y = a*k(-1)^theta*h^(1-theta);
a = (1-rho)*aa+rho*a(-1) + e;
y = c + i;
eta*k = (1-delta)*k(-1) + i;
gam*c*h = (1-theta)*y;
eta/c = bet*(1/c(+1))*(theta*(y(+1)/k)+1-delta);
end;

initval;
a = 1.7;
y = 600;
c = 500;
k = 3000;
i = 100;
h = 200;
end;

steady;
