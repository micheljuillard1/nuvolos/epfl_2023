var y a k c i h;
varexo e;

parameters theta rho eta gamma beta delta aa;

beta = 0.99;
delta = 0.025;
theta = 0.2;
rho = 0.9959; 
eta = 1.0051;  
gamma = 0.0045;
aa = 1.8;

model;
y = a*k(-1)^theta*h^(1-theta);
a = (1-rho)*aa+rho*a(-1) + e;
y = c + i;
k = (1-delta)*k(-1)/eta + i;
gamma*c*h = (1-theta)*y;
eta/c = beta*(1/c(+1))*(theta*(y(+1)/k)+1-delta);
end;

initval;
y = 0.5;
a = 0.5;
c = 0.5;
k = 0.5;
h = 0.5;
i = 0.5;
end;

gamma = 1;
steady;

context.work.initval_endogenous .=   context.results.model_results[1].trends.endogenous_steady_state;

gamma = 0.5;
steady; 
