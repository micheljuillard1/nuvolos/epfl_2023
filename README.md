# Analyzing DSGE models with DynareJulia

## Location
- EPFL, Lausanne

## Dates
- Tuesday, March 2, 2023 - March 5, 2023

## Instructor
- Michel Juillard, Banque de France

## Syllabus

- May 2, 2023

	– Introduction to DSGE models

	– Introduction to Julia

	– Introduction to Dynare
	
	– Perfect foresight simulations
	
	– Hand on exercises

- May 3, 2023

	– Local approximation and simulations

	– Hand on exercises

- May 4, 2023

	– Introduction to Bayesian estimation of DSGE models

	– Hand on exercises

- May 5, 2023 (until 16:30)

	– Estimation in DynareJulia

	– Hand on exercises

